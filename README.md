# ElemBdForm

[TOC]

Doc and README still Work In Progress

## Introduction

This project is a Javascript library to build HTML forms easily and retrieve the submited result as a JSON object **on client side**. In other words, it allows you to create a JSON value from user input easily through HTML forms generated from template.

You can create a form template in JSON, and generate the corresponding form inside your html page. The template define your own form html structure with your fields and a sumbit value template. Getting the value of the form will gather values from your fields to create the submit value which is a JSON object. Then you can use your value as you want !

In addition, you can define the behavior of your fields from other fields state. For example, make a fields required when an other has a given value, or show a field only when an other has a given value. (see more [here](#field-bindings))

This project uses [elem-bd][ext.3] 

## Requirements

They are few requirements concerning form fields (see [How to build one](#)) :
 - They should be HtmlElements (As W3C spec).
 - The value of the fields should be accesible throug a `value` property.

Then some methods of the form assume behaviors of form fields :
 - The `clear()` method assume that fields has a `clear` method that clears their value.   
 - The `disable()` method assume that fields can be disabled toggling the `disabled` attribute on the corresponding HtmlElement.    
 - The `readOnly()` method assume that fields can be disabled toggling the `readonly` attribute on the corresponding HtmlElement.

## Philosophy

This library is aimed to be used with [customElement](https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements) for the fields.

This library does not impose any html structure or styling. It is based on the [elem-bd][ext.3] library for the html templating. All of the html structure and elements classes for styling is defined in your template.

Moreover, thanks to [elem-bd][ext.3] you can make your form modular. For example, make it multilingual and give it dynamic ressources. Dynamic ressources are usefull when you want to fetch data and feed it to your form before building it. (see [example](#external-enums)) 

## Get started

### Decalaration

This part shows you how to declare a new form in "non-strict" mode and "strict" mode. After this part, the following code will be written in "strict" mode but you are free to use "non-strict" mode the same way. 

#### Mode non-strict

Html

```html
<custom-form id="custom_form"></custom-form>

<script type="module" src="./ElemBdForm
ElemBdForm.js"></script>
<script>
// get custom_from
let custom_form = document.getElementById('custom_form');

// do your stuff
...
</script>
```

#### Mode strict

Html

```
<custom-form id="custom_form"></custom-form>

<script type="module" src="./index.js"></script>
```

Javascript

```js
"use strict"; // index.js

import "./ElemBdForm
ElemBdForm.js";

// get custom_from
let custom_form = document.getElementById('custom_form');

// do your stuff
...
```

### Generate the form


```js
import "./ElemBdForm
ElemBdForm.js"; 

var my_form = document.getElementById('my-form');

my_form.setTemplate(my_form_template); 
```

## Docs

⚠ This doc is still work in progress

1. [Custom Form][1.0]
2. [CustomForm.disable()][1.1]
3. [CustomForm.clear()][1.2]
4. [CustomForm.readOnly()][1.3]
5. [CustomForm.setTemplate()][1.4]

[ext.1]: https://developer.mozilla.org/fr/docs/Web/API/HTMLElement "HtmlElement"
[ext.2]: https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements "Using Custom Element"
[ext.3]: https://gitlab.com/florian.desmortreux/elem-bd.git "elem-bd"

[1.0]: ./docs/CustomForm.md              "Custom Form"
[1.1]: ./docs/CustomForm.disable.md      "CustomForm.disable()"
[1.2]: ./docs/CustomForm.clear.md        "CustomForm.clear()"
[1.3]: ./docs/CustomForm.readOnly.md     "CustomForm.readOnly()"
[1.4]: ./docs/CustomForm.setTemplate.md  "CustomForm.setTemplate()"