# CustomForm.clear()

Clear the form by clearing all the values of the fields.

Call the `clear()` on each controllable element

#### Syntaxe

```js
clear()
```