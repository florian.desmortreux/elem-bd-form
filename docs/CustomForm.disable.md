# CustomForm.disable()

Disable the [CustomForm]

Disable all fields of the form by toggling on the `disabled` attribute on each controllable elements of the [htmlController](#htmlController)

### Syntaxe

```js
disable()
disable(state)
```

#### Parameters

<dl><dt>

`state` : Boolean</dt><dd>

Default `true`. If `false` enable the form .</dd>

</dl>
