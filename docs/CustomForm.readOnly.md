# CustomForm.readOnly()

Turn all fields of the form read only by toggling on the `readonly` attribute on each controllable elements of the [htmlController](#htmlController) 

#### Syntaxe

```js
readOnly()
readOnly(state)
```

#### Parameters

<dl><dt>

`state`</dt><dd>

Boolean. Set the read only state of the form to the given `state`. Default is `true`

</dd></dl>