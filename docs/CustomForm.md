## Custom Form

The class of the HTML `<custom-form></custom-form>` element.
See ([HtmlElement][ext.1]). It has all the methods to manage the lifecycle of the form.

It is a [Custom Element][ext.2].

### Declaration

#### Mode non-strict

Html

```html
<custom-form id="custom_form"></custom-form>

<script type="module" src="./ElemBdForm.js"></script>
<script>
// get custom_from
let custom_form = document.getElementById('custom_form');

// do your stuff
...
</script>
```

#### Mode strict

Html

```
<custom-form id="custom_form"></custom-form>

<script type="module" src="./index.js"></script>
```

Javascript

```js
"use strict"; // index.js

import "./ElemBdForm.js";

// get custom_from
let custom_form = document.getElementById('custom_form');

// do your stuff
...
```

### Properties

<dl><dt>

`value`</dt><dd>

The value of the form. You can get and set the value of the form through this property. See the getter and setter below.</dd><dt>

`htmlController`</dt><dd>

This is the controller given by the HtmlJsonTemplate when building the form. It is a javascript object with keys beeing the id of the controllable elements and values the corresponding HtmlElement. See [Controller](#)

</dd></dl>

### Getters and Setters

<dl><dt>

`get value()`</dt><dd>

Getting `value` create a new javascript object from [sumbitTemplate](#) and the values of the fields. 

</dd><dt>

`set value()`</dt><dd>

Setting `value` will fill the form fields with the value according to the [sumbitTemplate](#).

</dd></dl>

### Methods

<dl><dt>

`setTemplate()`</dt><dd>

This method allow to create a form inside the custom-form element from a [elem-bd][ext.3] template.</dd><dt>

`clear()`</dt><dd>

Clear the form by clearing all the values of the fields.</dd><dt>

`disable()`</dt><dd>

Disable all fields of the form by toggling on the `disabled` attribute on each controllable elements of the [htmlController](#htmlController).</dd><dt>

`readOnly()`</dt><dd>

Turn all fields of the form read only by toggling on the `readonly` attribute on each controllable elements of the [htmlController](#htmlController) 

</dd></dl>

[ext.1]: https://developer.mozilla.org/fr/docs/Web/API/HTMLElement "HtmlElement"
[ext.2]: https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements "Using Custom Element"
[ext.3]: https://gitlab.com/florian.desmortreux/elem-bd.git "elem-bd"

[1.0]: ./CustomForm.md              "Custom Form"
[1.1]: ./CustomForm.disable.md      "CustomForm.disable()"
[1.2]: ./CustomForm.clear.md        "CustomForm.clear()"
[1.3]: ./CustomForm.readOnly.md     "CustomForm.readOnly()"
[1.4]: ./CustomForm.setTemplate.md  "CustomForm.setTemplate()"