# CustomForm.setTemplate()

This method allow to create a form inside the custom-form element from a [`elem-bd`][ext.3] template.

### Syntaxe

```js
custom_form.setTemplate(template)
custom_form.setTemplate(template, fragmentSet)
 ```

### Parameters
<dl><dt>

`template`</dt><dd>
    
A [template](#html-form-template). See its definition for more information about form templating

</dd><dt>

`fragmentSet`</dt><dd>

A single or a list of [fragmentSet](#). Defined as in the HtmlJsonTemplate module.

</dd></dl>


[ext.3]: https://gitlab.com/florian.desmortreux/elem-bd.git "elem-bd"